(ns entomology.core
  (:require [entomology.web :as w]
            entomology.atom
            entomology.category
            entomology.inventory
            entomology.user))

(defn -main [hostname port]
  (w/start-server hostname port (atom {})))
