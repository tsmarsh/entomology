(ns entomology.database
  (:require [entomology.model :as m]
            [digest :as d]))

(defprotocol DAO
  (create! [self object])
  (lookup [self id])
  (update! [self object])
  (upsert! [self object]))

(defprotocol Saveable
  (save-children! [self ^DAO db]))

(defprotocol Repository
    (find-by-id [self object])
    (find-by [self object search-fn])
    (list-by [self object finder-fn])

    (list-all [self object])
    (save! [self object]))

(defprotocol Idable
  (gen-id [self]))

(extend-type Object
  Idable
  (gen-id [self] (d/sha (str (dissoc self :id))))
  Saveable
  (save-children! [self _] self))
