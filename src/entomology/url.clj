(ns entomology.url)

(defprotocol URLable
  (path [self][self parent]))
