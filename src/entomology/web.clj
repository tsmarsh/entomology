(ns entomology.web
  (:require [compojure.route :as r]
            [compojure.handler :as h]
            [compojure.core :as c]
            [org.httpkit.server :as hk]
            [clojure.data.json :as json]
            [entomology.model :as m]
            [entomology.database :as e-db]
            [entomology.url :as url]))

(defn get-url [object hostname port]
  (format "http://%s:%d/%s" hostname port (url/path object)))

(defn successful-response
  [object]
  (fn [ring-request]
    (hk/with-channel ring-request channel
      (if (hk/websocket? channel)
        (hk/on-receive channel (fn [data]
                            (hk/send! channel data)))
        (hk/send! channel {:status   200
                           :headers {"Content-Type" "application/javascript"}
                           :body    (json/write-str object)})))))

(defn redirect-response
  [object hostname port]
    (fn [ring-request]
      (hk/with-channel ring-request channel
        (if (hk/websocket? channel)
          (hk/on-receive channel (fn [data]
                              (hk/send! channel data)))
          (hk/send! channel {:status 302
                             :headers {"Location" (get-url object hostname port)}})))))

(def db (atom nil))
(def hostname (atom "localhost"))
(def port (atom 8080))


(c/defroutes app

  (c/context "/users/:user-id/inventories/:inventory-id" []
           (c/GET "/" [user-id inventory-id]
                  (successful-response (format "User: %s Inventory %s" user-id inventory-id)))
           (c/POST "/" [user-id inventory-id]
                   (successful-response (format "User: %s Inventory %s updated" user-id inventory-id))))

  (c/context "/users/:user-id/inventories" []
             (c/GET "/" [user-id]
                    (successful-response (format "User: %s All Inventories" user-id)))
             (c/POST "/" [user-id]
                     (successful-response (format "User: %s New Inventory" user-id))))

  (c/context "/users/:id" []
           (c/GET "/" [id]
                  (successful-response (format "User: %s" id)))
           (c/POST "/" [id]
                   (successful-response (format "User %s Info Updated"))))

  (c/context "/users" []
             (c/GET "/" []
                    (successful-response []))
             (c/POST "/" {body :body}
                     (redirect-response (e-db/save! @db (m/map->User (json/read-str (slurp body) :key-fn keyword)))
                                        @hostname
                                        @port)))

  (r/files "/static/")
  (r/not-found "<p>Page not found.</p>"))


(defonce server (atom nil))

(defn stop-server []
  (when-not (nil? @server)
    (@server :timeout 100)
    (reset! server nil)
    (println "Server stopped")))

(defn start-server [h p database]
  (reset! hostname h)
  (reset! port p)
  (reset! db database)
  (reset! server (hk/run-server #'app {:port @port}))
  (println "Server running on" @port))
