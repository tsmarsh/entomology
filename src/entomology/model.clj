(ns entomology.model)

(defrecord Category [id
                     ^String fullname
                     ^String name
                     parent])

(defrecord Type [id name])

(defrecord Inventory [id
                      ^Category category
                      ^Type type
                      value])

(defrecord User [id name inventories])
