(ns entomology.atom
  (:require [entomology.database :as d])
  (:import [clojure.lang Atom]))

(defn upsert-database! [sym db]
                  (if-not (contains? @db sym)
                      (swap! db assoc sym (atom {})))
                  (sym @db))

(defn record-to-keyword [record]
  (->> record type .getSimpleName .toLowerCase .trim keyword))


(defprotocol DatabaseFinder
  (get-database [self db]))

(extend-protocol DatabaseFinder
  Object
  (get-database
   [self db]
   (upsert-database! (record-to-keyword self) db)))

(extend-type Atom
  d/DAO
  (create!
    [self object]
    (let [db (get-database object self)
          value (d/save-children! object self)
          id (d/gen-id value)
          new-value (assoc value :id id)]
      (swap! db assoc id new-value)
      new-value))

  (lookup
     [self object]
     (get @(get-database object self) (:id object)))

  (update!
   [self object]
     (if (contains? @(get-database object self) (:id object))
       (d/create! self object)
       (throw (RuntimeException. (str "Object does not exist: " (print-str object))))))

  (upsert!
   [self object]
   (d/create! self object))

  d/Repository

  (find-by-id [self object]
               (let [db (get-database object self)]
                 (db (:id object))))

  (list-all [self object]
            (let [db @(get-database object self)]
              (vals db)))

  (list-by [self object finder-fn]
     (filter (finder-fn object) (d/list-all self object)))

  (find-by [self object search-fn]
             (first (d/list-by self object search-fn)))

  (save! [self object]
        (if (nil? (:id object))
          (d/create! self object)
          (d/update! self object))))
