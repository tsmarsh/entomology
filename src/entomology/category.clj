(ns entomology.category
  (:require [entomology.database :as d]
            [clojure.string :as s])
  (:import [entomology.model Category]))

(defn get-name [category]
  (if-not (nil? (:fullname category))
    (last (s/split (:fullname category) #"\."))))

(defn get-parents-name [category]
  (if-not (nil? (:fullname category))
    (last (re-matches #"(.*)\..*$" (:fullname category)))))

(defn find-by-fullname [object]
  (fn [cat]
      (= (:fullname cat) (:fullname object))))

(defn get-parent
  [category db]
  (let [proto-parent (Category. nil (get-parents-name category) nil nil)
        looked-up-parent (d/find-by db proto-parent find-by-fullname)]
    (if looked-up-parent
      looked-up-parent
      (if (:fullname proto-parent)
        (d/upsert! db proto-parent)))))

(extend-type Category
  d/Saveable
  (save-children!
     [self db]
     (let [parent (get-parent self db)]
       (assoc self :name (get-name self)
                   :parent parent))))
