(ns entomology.user
  (:require [entomology.database :as d]
            [entomology.url :as url])
  (:import [entomology.model User]))

(defn find-by-name [object]
  (fn [user]
    (= (:name object) (:name user))))

(extend-type User
  d/Saveable
  (d/save-children!
     [self db]
     (assoc self :inventories
                 (map (partial d/upsert! db) (:inventories self))))

  url/URLable
  (url/path [self]
        (format "users/%s/%s" (:name self) (:id self))))

