(ns entomology.inventory
  (:require [entomology.database :as d]
            [entomology.url :as url])
  (:import [entomology.model Category Inventory Type]))

(defn get-type [inv db]
  (if (:type inv)
      (d/upsert! db (:type inv))))

(extend-type Inventory
  d/Saveable
  (save-children!
     [self db]
     (assoc self :category (d/upsert! db (:category self))
                 :type (get-type self db)))
  url/URLable
  (path [self]
        (format "inventories/%s" (:fullname self)))
  (path [self parent]
        (format "users/%s/%s/inventories/%s/%s"
                (:name parent) (:id parent)
                (:fullname self)(:id self))))
