# entomology

A Clojure port of InventoryServer.

Clojure was my first choice for InventoryServer, but I felt that proving that a [PeST](http://tailoredshapes.com/rest/web/2013/04/21/pest.html) was possible in Java would prevent people thinking that the protocol required some magical, functional backend.

What I am hoping this will prove is that, whilst it doesn't require a magical, functional backend, it sure does help.

## Installation

*Requires [leiningen](http://leiningen.org/)*

Clone the repo

  lein test #will run the test suite



##Todo

* A web front end
* Integrate with [Datomic](http://www.datomic.com/)


##Extra credit

* Create a transaction log with a view to making the 'in-memory' database persistent.



