(ns entomology.category-test
  (:use clojure.test)
  (:require [entomology.category :as c]
            [entomology.atom :as a]
            [entomology.database :as d])
  (:import [entomology.model Category]))


(deftest category
  (testing "c/get-name Doesn't shit the bed if the category is null"
    (is (nil? (c/get-name (Category. nil nil nil nil)))))

  (testing "A categories name is just its fullname if there is not ."
      (is (= "test"
             (c/get-name (Category. nil "test" nil nil)))))

  (testing "We can derive a Categories name from its full name"
      (is (= "test"
             (c/get-name (Category. nil "com.test" nil nil)))))

  (testing "c/get-parents-name Doesn't shit the bed if the category is null"
    (is (nil? (c/get-parents-name (Category. nil nil nil nil)))))

  (testing "A category with no parent has no parents name"
      (is (nil?
             (c/get-parents-name (Category. nil "com" nil nil)))))

  (testing "We can derive a Categories parents name from its full name"
      (is (= "com"
             (c/get-parents-name (Category. nil "com.test" nil nil)))))

  (testing "We can compare categories by fullname"
      (let [good (Category. nil "com.test" nil nil)
            bad (Category. nil "com.bad" nil nil)]
        (is (= true ((c/find-by-fullname good) good)))
        (is (= false ((c/find-by-fullname good) bad))))))


(deftest category-and-model
  (testing "c/find-by-fullname finds a saved category from its fullname"
        (let [object (Category. nil "com.test" nil nil)
              db     (atom {:category (atom {"someid"  (Category. "someid" "com.test" nil nil)
                                             "otherid" (Category. "otherid" "not.test" nil nil)})})]
          (is (= (Category. "someid" "com.test" nil nil)
                 (d/find-by db object c/find-by-fullname)))))

  (testing "c/find-by-fullname behaves correctly when it can't find by fullname"
      (let [object (Category. nil "com.nope" nil nil)
            db     (atom {:category (atom {"someid"  (Category. "someid" "com.test" nil nil)
                                           "otherid" (Category. "otherid" "not.test" nil nil)})})]
        (is (= nil
               (d/find-by db object c/find-by-fullname))))))

(deftest category-and-save-children
  (testing "d/save-children! does nothing when there is no parent"
    (let [object (Category. nil "base" nil nil)
          db (atom {:category (atom {})})
          current-db @db]
      (d/save-children! object db)
      (is (= current-db @db))))

  (testing "d/save-children! does nothing when there is no parent"
    (let [object (Category. nil "base" nil nil)
          db (atom {})]

      (is (= "base"(:name (d/save-children! object db))))))

  (testing "c/get-parent returns a saved parent if none found in db"
      (let [child (Category. nil "base.child" nil nil)
            db (atom {:category (atom {})})
            parent (c/get-parent child db)]
      (is (= "base" (:fullname parent)))
      (is (not (nil? (:id parent))))))

  (testing "c/get-parent returns a saved parent if parent in db"
      (let [child (Category. nil "base.child" nil nil)
            parent (Category. 15123 "base" "base" nil)
            db (atom {:category (atom {15123 parent})})]
      (is (= parent (c/get-parent child db)))))

  (testing "d/save-children! finds the correct parent from the db"
    (let [child (Category. nil "base.child" nil nil)
          parent (Category. 12412 "base" "base" nil)
            db (atom {:category (atom {12412 parent})})]
      (is (= parent (:parent (d/save-children! child db))))))


  (testing "d/save-children! adds parent to the db if its missing"
    (let [child (Category. nil "base.child" nil nil)
            db (atom {:category (atom {})})]
      (d/save-children! child db)
      (is (= "base" (:name (last (first @(:category @db)))))))))


