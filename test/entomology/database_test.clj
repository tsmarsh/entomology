(ns entomology.database-test
  (:use clojure.test)
  (:require [entomology.database :as d])
  (:import (entomology.model Category)))

(defrecord Floop [truth])

(deftest idable
  (let [cat (Category. nil "fullname" nil nil)
        id "5dec62f6f2d4830d5944a99d5d06c3749fc2a5b4"]
    (testing "should default to sha for an object"
      (is (= id
             (d/gen-id cat))))
    (testing "should be idempotent"
      (is (= id
             (d/gen-id (Category. nil "fullname" nil nil)))))
    (testing "should ignore ids"
      (is (= id
             (d/gen-id (Category. "wrong id" "fullname" nil nil)))))
    (testing "shouldn't explode when there is no id on the object"
      (is (not (nil? (d/gen-id (Floop. "narp"))))))))
