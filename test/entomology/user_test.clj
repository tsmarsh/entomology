(ns entomology.user-test
  (:use clojure.test)
  (:require [entomology.user :as u]
            [entomology.atom]
            [entomology.inventory]
            [entomology.category]
            [entomology.database :as d])
  (:import [entomology.model User Inventory Category Type]))

(deftest user-save-children
  (testing "a user saves its inventories"
    (let [c  (Category. nil "test" nil nil)
          t  (Type. nil "test-type")
          i1 (Inventory. nil c t "test1")
          i2 (Inventory. nil c t "test2")
          u  (User. nil "Archer" [i1 i2])
          db (atom {})]
      (is (= 2 (count (filter #(not (nil? (:id %)))
                            (:inventories (d/save-children! u db))))))
      (is (= 2 (->> @db
                                     :inventory
                                     deref
                                     count))))))

(deftest user-find-by-name
  (testing "we can find users by their name"
    (let [u (User. nil "Archer" nil)]
      (is (= [u]
             (filter (u/find-by-name u)
                     [u (User. nil "Cassie" nil)]))))))

