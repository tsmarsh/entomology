(ns entomology.inventory-test
  (:use clojure.test)
  (:require [entomology.inventory :as i]
            [entomology.atom :as a]
            [entomology.category]
            [entomology.database :as d])
  (:import [entomology.model Inventory Category Type]))

(deftest inventory-save-children
  (testing "an inventory saves its category"
    (let [category (Category. nil "fullname" nil nil)
          inventory (Inventory. nil category nil nil)
          db (atom {})]
        (d/save-children! inventory db)
        (is (= "fullname" (->> @db
                              (:category)
                              (deref)
                              (first)
                              (last)
                              (:fullname))))))

  (testing "an inventory saves its type"
      (let [category (Category. nil "fullname" nil nil)
            t (Type. nil "type")
            inventory (Inventory. nil category t nil)
            db (atom {})]

          (d/save-children! inventory db)
          (is (= "type" (->> @db
                                (:type)
                                (deref)
                                (first)
                                (last)
                                (:name)))))))
