(ns entomology.atom-test
  (:use clojure.test)
  (:require [entomology.database :as d]
            [entomology.atom :as a]
            [entomology.category]
            [entomology.inventory])
  (:import [entomology.model Category Inventory Type]))


(def db (atom {}))
(def c (Category. nil "a.test" nil nil))
(def t (Type. nil "test-type"))
(def value (Inventory. nil c t "test value"))

(deftest saving-to-an-atom
  (let [id (d/gen-id value)
        saved (d/create! db value)]

    (testing "By default it will save and return the value with an id"
      (is (= (assoc value :id id)
             saved)))

    (testing "The value is saved in the database"
      (is (= saved
             (@(a/get-database value db) id))))

    (testing "You can find the value from its id"
      (is (= (assoc value :id id)
             (d/lookup db (assoc value :id id)))))))

(deftest updating-an-atom
  (let [id (d/gen-id value)
        saved (d/create! db value)
        new-value (Inventory. (:id saved) c t "an updated value")
        new-id    (d/gen-id new-value)]
    (testing "You can update a value... kinda"
      (is (= (assoc new-value :id new-id)
             (d/update! db new-value))))

    (testing "The old value is still nice and safe in the database"
      (is (= (assoc value :id id)
             (@(a/get-database value db) id))))

    (testing "And the new value is in there too"
      (is (= (assoc new-value :id new-id)
             (@(a/get-database new-value db) new-id))))))
