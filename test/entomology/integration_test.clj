(ns entomology.integration-test
  (:use clojure.test)
  (:require [entomology.web :as w]
            [clojure.data.json :as json]
            [org.httpkit.client :as hc]
            entomology.inmemory)
  (:import [entomology.model User]))

(def db (atom {}))

(defn fixtures
  [c]
  (w/start-server "localhost" 6666 db)
  (try (c)
    (catch Exception e (str "caught exception: " (.getMessage e)))
    (finally (w/stop-server))))

(def options {:timeout 200})

(deftest user
  (fixtures
     #(do
        (testing "There are no users in the database"
          (is (= []
                 (json/read-str (slurp (:body @(hc/get "http://localhost:6666/users" options))))))

        (testing "Adding an User to the database"
          (is ((complement nil?)(re-matches #"http://localhost:6666/users/Archer/.*"
               (->> (assoc options :body (json/write-str (User. nil "Archer" nil))
                                   :follow-redirects false)
                    (hc/post "http://localhost:6666/users")
                    deref
                    :headers
                    :location)))))

        ))))

