(defproject entomology "0.0.1-SNAPSHOT"
  :description "Cool new project to do things and stuff"
  :dependencies [[org.clojure/clojure "1.7.0-alpha2"]
                 [javax.servlet/servlet-api "2.5"]
                 [compojure "1.2.0"]
                 [http-kit "2.1.16"]
                 [digest "1.4.4"]
                 [org.clojure/data.json "0.2.5"]]
  :main entomology.core)

